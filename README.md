# Docker image utilizing with a LEMP stack

LEMP stack: Linux, nginx, MariaDB, PHP

This image only provides a basic LEMP stack, i.e. only using HTTP (not HTTPS)

## Usage

### Build

`docker build -t docker-lemp .`

### Start (detached)

`docker run --name="lemp" -dit -p 80:80 lemp /bin/bash`

*Remarks:* Note the `/bin/bash` at the end of the start command. This leads the container in a running state when it is started in detached mode.

Test if the container is running correctly: Just enter http://localhost/phpinfo.php in your browser. The PHP info page should be shown.
