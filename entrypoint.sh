#!/bin/bash

# Configure nginx
sed -i 's/^worker_processes.*/worker_processes auto;/' /etc/nginx/nginx.conf
sed -i 's/^user.*/user www-data;/' /etc/nginx/nginx.conf

# Set permissions
chown -R www-data:www-data /var/www

#echo "Starting services..."
service php7.2-fpm start
service mysql start
service nginx start

# Execute command (in order to let the container stay running in detached mode)
exec "$@";