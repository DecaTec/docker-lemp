FROM ubuntu:18.04
LABEL maintainer "support@decatec.de"

ENV DEBIAN_FRONTEND noninteractive

RUN apt update \
	&& apt-get upgrade -y \
	&& apt-get install -y  \
    wget \
    gnupg2

RUN wget -O - http://nginx.org/keys/nginx_signing.key | apt-key add - \
	&& echo 'deb [arch=amd64] http://nginx.org/packages/mainline/ubuntu/ bionic nginx' >> /etc/apt/sources.list.d/nginx.list \
	&& echo 'deb-src [arch=amd64] http://nginx.org/packages/mainline/ubuntu/ bionic nginx' >> /etc/apt/sources.list.d/nginx.list \
	&& apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8 \
	&& echo 'deb [arch=amd64] http://ftp.hosteurope.de/mirror/mariadb.org/repo/10.3/ubuntu bionic main' >> /etc/apt/sources.list.d/MariaDB.list \
	&& echo 'deb-src http://ftp.hosteurope.de/mirror/mariadb.org/repo/10.3/ubuntu bionic main' >> /etc/apt/sources.list.d/MariaDB.list \
	&& apt-get update \
	&& apt-get install -y  \
	nginx \
	mariadb-server \
	php-fpm \
	php-gd \
	php-mysql \
	php-curl \
	php-xml \
	php-zip \
	php-intl \
	php-mbstring \
	php-bz2 \
	php-json \
	&& apt autoremove

COPY entrypoint.sh /entrypoint.sh
COPY files/etc/nginx /etc/nginx
COPY files/var/www /var/www

VOLUME ["/var/www/"]

EXPOSE 80 443

ENTRYPOINT ["/bin/bash", "./entrypoint.sh"]